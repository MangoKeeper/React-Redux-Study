import React, { Component } from 'react';

export default class Counter extends Component {

	constructor(props){
		super(props);
		this.reduceClick=this.reduceClick.bind(this);
		this.addClick=this.addClick.bind(this);
    	this.state = {
      		count: 0
    	}
	}

	addClick(){
		this.updateCounter(true);
	}

	reduceClick(){
		this.updateCounter(false);
	}
	
	updateCounter(b){
		const prevValue=this.state.count;
		console.log("prevValue:"+prevValue);
		const nextValue=b?prevValue+Math.ceil(Math.random()*10):prevValue-Math.ceil(Math.random()*10);
		this.setState({
			count:nextValue
		});
		console.log("nextValue:"+nextValue);
		this.props.onUpdate(prevValue,nextValue);
	}

	render() {
		return (
			<div>
				<button onClick={this.reduceClick}>-</button>
				Click Count:{this.state.count}
				<button onClick={this.addClick}>+</button>
			</div>
		);
	}
}

Counter.defaultProps = {
	initValue: 0,
	onUpdate:f => f
};
