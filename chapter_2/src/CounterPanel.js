import React, { Component } from 'react';
import Counter from "./Counter";

export default class CounterPanel extends Component {

	constructor(props){
		super(props);
		this.updateSum=this.updateSum.bind(this);
		//this.initValues = [ 0, 0];

    	//const initSum = this.initValues.reduce((a, b) => a+b, 0);
    	this.state = {
      		sum: 0
   		};
	}

	updateSum(prevValue,newValue){
		const valueChange = newValue - prevValue;
		const nowValue=this.state.sum;
		console.log("state sum1:"+nowValue);
    	this.setState({ sum: this.state.sum + valueChange});

	}

	render() {
		return (
			<div>
				<Counter onUpdate={this.updateSum}/>
				<Counter onUpdate={this.updateSum} />
				<span>Total Sum:{this.state.sum}</span>
			</div>
		);
	}


	componentDidUpdate(prevProps, prevState) {
		const changeValue=this.state.sum;
    	console.log("state sum2:"+changeValue);
	}
}
