import React, { Component } from 'react';

export default class Counter extends Component {

	constructor(props){
		super(props);
		this.addClick=this.addClick.bind(this);
		this.reduceClick=this.reduceClick.bind(this);
		this.state = {
			count:0
		}
	}

	addClick(){
		this.setState({count:this.state.count+1});
	}
	
	reduceClick(){
		if(this.state.count>0)
			this.setState({count:this.state.count-1});
	}

	render() {
		const divStyle={
			margin:'200px'
		}
		return (
			<div style={{divStyle}}>
				<button onClick={this.reduceClick}>-</button>{this.props.title} 当前计数值为：{this.state.count}<button onClick={this.addClick}>+</button>
			</div>
		);
	}
}
