import React, { Component } from 'react';
import Counter from './Counter';

export default class CounterPanel extends Component {
	render() {
		return (
			<div>
				<Counter title="Counter1"/>
				<Counter title="Counter2"/>
			</div>

		);
	}
}
